﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace TestTask_MVC.HtmlHelpers
{
	public static partial class HtmlExtensions
	{
		public static MvcHtmlString ClientIdFor<TModel, TProperty>( this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression )
		{
			return MvcHtmlString.Create(
				htmlHelper.ViewContext.ViewData.TemplateInfo.GetFullHtmlFieldId( ExpressionHelper.GetExpressionText( expression ) ) );
		}
	}
}