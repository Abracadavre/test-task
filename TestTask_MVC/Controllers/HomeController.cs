﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Spatial;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TestTask_MVC.DAL;
using TestTask_MVC.Models;

namespace TestTask_MVC.Controllers
{
	public class HomeController : Controller
	{
		private ConstructionsContext _context = new ConstructionsContext();

		public ActionResult Index()
		{
			var constructions = _context.Constructions.ToArray();
			return View( constructions );
		}

		public ActionResult AddNewConstruction()
		{
			return View();
		}

		[HttpPost]
		public ActionResult AddNewConstruction( AdvertisingConstruction construction )
		{
			if( ModelState.IsValid )
			{
				_context.Constructions.Add( construction );
				_context.SaveChanges();

				return RedirectToAction( "Index" );
			}

			return View( construction );
		}

		public ActionResult DeleteConstruction( long constructionID )
		{
			var construction = _context.Constructions.FirstOrDefault( c => c.ID == constructionID );
			if( construction != null )
			{
				_context.Constructions.Remove( construction );
				_context.SaveChanges();
			}
			return RedirectToAction( "Index" );
		}

		public ActionResult EditConstruction( long constructionID )
		{
			var construction = _context.Constructions.FirstOrDefault( c => c.ID == constructionID );
			return View( construction );
		}

		[HttpPost]
		public ActionResult EditConstruction( AdvertisingConstruction construction )
		{
			var existingConstruction = _context.Constructions.FirstOrDefault( c => c.ID == construction.ID );

			if( existingConstruction != null )
			{
				existingConstruction.Description = construction.Description;
				existingConstruction.MaintenanceTime = construction.MaintenanceTime;
				existingConstruction.Location = construction.Location;
				existingConstruction.Type = construction.Type;
				existingConstruction.Height = construction.Height;
				existingConstruction.Width = construction.Width;
				existingConstruction.MonthlyCost = construction.MonthlyCost;

				_context.SaveChanges();
			}

			return RedirectToAction( "Index" );
		}

		public ActionResult ViewConstruction( long constructionID )
		{
			var construction = _context.Constructions.FirstOrDefault( c => c.ID == constructionID );

			if( construction != null )
			{
				return View( construction );
			}
			return View( "Index" );
		}
	}
}
