﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using TestTask_MVC.DAL;
using TestTask_MVC.Models;

namespace TestTask_MVC.Controllers
{
	public class ConstructionsController : ApiController
	{
		private ConstructionsContext _context = new ConstructionsContext();
		public ICollection<AdvertisingConstruction> Get()
		{
			return _context.Constructions.ToArray();
		}

		public AdvertisingConstruction Get( long id )
		{
			return _context.Constructions.FirstOrDefault( c => c.ID == id );
		}

		public HttpResponseMessage Put( AdvertisingConstruction construction )
		{
			if( _context.Constructions.FirstOrDefault( c => c.ID == construction.ID ) != null )
			{
				return new HttpResponseMessage( HttpStatusCode.BadRequest );
			}

			_context.Constructions.Add( construction );
			_context.SaveChanges();

			return new HttpResponseMessage( HttpStatusCode.Created );
		}

		public string Delete( long id )
		{
			var toDelete = _context.Constructions.FirstOrDefault( c => c.ID == id );

			if( toDelete != null )
			{
				_context.Constructions.Remove( toDelete );
				_context.SaveChanges();

				return String.Format( "Construction ID {0} was succesfully removed from database", id );
			}

			else
				return String.Format( "Cannot find construction with ID {0}", id );
		}
	}
}
