﻿(function () {
    var initialize = function (i, el) {
        var $input = $(el);

        var $map = $('<div>', {
            css: {
                width: '400px',
                height: '400px'
            }
        }).insertAfter($input);

        var latLong = parseLatLong(this.value);

        if (!latLong || !latLong.latitude || !latLong.longitude) {
            latLong = {
                latitude: 40.716948,
                longitude: -74.003563
            };
        }

        var position = new google.maps.LatLng(latLong.latitude, latLong.longitude);

        var map = new google.maps.Map($map[0], {
            zoom: 14,
            center: position,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            maxZoom: 14
        });

        var marker = new google.maps.Marker({
            position: position,
            map: map
        });

        var updateMarker = function (updateEvent) {
            marker.setPosition(updateEvent.latLng);

            map.panTo(updateEvent.latLng);

            $input.val(marker.getPosition().toUrlValue(13));
        };

        if ($input.hasClass('editor-for-dbgeography')) {
            google.maps.event.addListener(map, 'click', updateMarker);

            $input.on('change', function () {
                var latLong = parseLatLong(this.value);

                latLong = new google.maps.LatLng(latLong.latitude, latLong.longitude);

                updateMarker({ latLng: latLong });
            });
        }
    };

    var parseLatLong = function (value) {
        if (!value) { return undefined; }

        var latLong = value.match(/-?\d+\.\d+/g);

        return {
            latitude: latLong[0],
            longitude: latLong[1]
        };
    };

    $('.editor-for-dbgeography, .display-for-dbgeography').each(initialize);
})();