﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Spatial;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TestTask_MVC
{
	public class DbGeographyModelBinder : IModelBinder
	{
		public object BindModel( ControllerContext controllerContext, ModelBindingContext bindingContext )
		{
			var valueProviderResult = bindingContext.ValueProvider.GetValue( bindingContext.ModelName );
			return BindModelImpl( valueProviderResult != null ? valueProviderResult.AttemptedValue : null );
		}

		private DbGeography BindModelImpl( string value )
		{
			if( value == null )
			{
				return (DbGeography)null;
			}
			string[] latLongStr = value.Split( ',' );
			string point = string.Format( "POINT ({0} {1})", latLongStr[1], latLongStr[0] );
			//4326 format puts LONGITUDE first then LATITUDE
			DbGeography result = DbGeography.FromText( point, 4326 );
			return result;
		}
	}

	public class EFModelBinderProviderMvc : System.Web.Mvc.IModelBinderProvider
	{
		public IModelBinder GetBinder( Type modelType )
		{
			if( modelType == typeof( DbGeography ) )
				return new DbGeographyModelBinder();
			return null;
		}
	}
}