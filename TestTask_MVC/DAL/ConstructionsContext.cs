﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using TestTask_MVC.Models;

namespace TestTask_MVC.DAL
{
	public class ConstructionsContext : DbContext
	{
		public DbSet<AdvertisingConstruction> Constructions { get; set; }
	}
}