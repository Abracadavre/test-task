﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;
using System.Linq;
using System.Web;

namespace TestTask_MVC.Models
{
	public class AdvertisingConstruction
	{
		[Key]
		[DatabaseGenerated( DatabaseGeneratedOption.Identity )]
		public long ID { get; set; }

		[Required]
		[Display( Name = "Description" )]
		public string Description { get; set; }

		[Required]
		[Display( Name = "Maintenance Time" )]
		public DateTime MaintenanceTime { get; set; }

		[Required]
		[Display( Name = "Location" )]
		public DbGeography Location { get; set; }

		[Required]
		[Display( Name = "Type" )]
		public ConstructionType Type { get; set; }

		[Required]
		[Display( Name = "Height" )]
		public decimal Height { get; set; }

		[Required]
		[Display( Name = "Width" )]
		public decimal Width { get; set; }

		[Required]
		[Display( Name = "Monthly Cost" )]
		public decimal MonthlyCost { get; set; }
	}
}