﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestTask_MVC.Models
{
	public enum ConstructionType : byte
	{
		Billboard = 1,
		Citylight = 2,
		VideoScreen = 3
	};
}